Computer Vision (Drishti)
=========================

This module contains computer-vision specific code.

Drishti Library
---------------

This python library aims to ease the use of various computer-vision tasks, datasets and challenges.

Usage
-----

To install use 
``` pip install drishti ```

Refer to the class docstrings for usage.


Current capabilities
--------------------

1. Flexible and convenient plotting of object detection datasets.
2. Easily parse and load the PascalVOC object-detection dataset
 
